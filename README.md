This site is for testing and trying various technologies primarily using Decap CMS using Netlify's git-gateway on a GitLab repository.

It was named Hugo 120 because it was made at the same time that Hugo released version v0.120 of the static site generator this site was built with.

Other things to experiment with:

- Decap CMS using git-gateway on Netlify ✔
- Using multiple media folders in Decap.
- Setting the media folder to Hugo's assets directory. ✔
- Redirects using Netlify, Hugo and .htaccess
- Using .htaccess on Netlify
- Setting up Dart SASS with Hugo on Netlify.
- Using CSS subgrid, which finally became available in all the main browsers.
- Make better use of CSS Custom Properties [Ishadeed style] and [inline Custom Properties].
- Other recent features on the newer Hugo versions such as image processing
- Try out the new default theme built into Hugo and created with `hugo new theme <theme-name>`.
- Try out Hugo's JS Build, the built in ES Build for Javascript bundling.
- Try out Hugo modules. Something I've not used much before.

 


 [Ishadeed style]: https://ishadeed.com/article/css-vars-101/
 [inline Custom Properties]: https://ishadeed.com/article/css-variables-inline-styles/