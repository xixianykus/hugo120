---
layout: blog
title: Post from the CMS
date: 2023-11-10T16:41:21.933Z
page_img: /img/perspective grid man.png
rating: 5
tags: [Decap CMS, Gitlab]
---

So trying a new access token from GitLab, this time with the level of owner instead of Guest. Didn't put a date so got the maximum, which is not for ever, just a measly 12 months.