---
date: "2023-11-12T19:52:10Z"
description: Ideas for the future of this site.
draft: false
linktitle: Going Forward
page_img: ""
summary: Some thoughts on additions for this site.
tags:
- untagged
title: Going Forward from v0.2
---


Well today marked milestone for this site, going from v0.1 to v0.2. The styling was completely redone and the main parts of the site now have a very different, subgrid layout.

Of course there's no end to the kind of things you can do with a web site. Here's just a few that come to mind. Some because they're cool, and others because by implementing them I feel I'll learn something.

1. Well the home page is looking quite cool though I think the background could be improved. Perhaps a large circle behind the figure, Perhaps some animations going on.
2. The News section is currently a bit of disaster that needs sorting out. This too was done with a subgrid layout but I was trying to automate some stuff with Hugo which was trickier than I'd expected. The main problem is that you will probably need to go through each layout by hand to optimize where everything goes. This is because every article is different.
3. Getting the site working with SASS would be could. I already have one site that broke when Hugo moved from embedded Dart Sass to just Dart Sass. This happened at Hugo v.0.114 iirc.
4. I could create more figures to add interest. One for each section perhaps.
5. Next and Previous buttons
6. A dark mode
7. Get some image processing going from the `/assets/` directory.
8. Tidy up some of the articles.
9. Add some animation to the main menu and other links.
10. Try some other subgrid layouts out. I could try the CSS
11. Try other CSS technologies. Container Queries are a big one I haven't go around to trying yet.  Martine Dowden
made a good, [succinct video] on these.





[succinct video]: https://www.youtube.com/watch?v=TC6iPi1nY8Y