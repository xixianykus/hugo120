+++
title = 'Hugo Modules'
date = 2023-11-14T07:54:49Z
draft = false
description = "An intro to Hugo modules"
summary = "Some notes on using Hugo's modules feature."
tags = ["Hugo"]
page_img = ""

[[links]]
name = "Hugo Modules: Everything You Need to Know"
url = "https://www.thenewdynamic.com/article/hugo-modules-everything-from-imports-to-create/"
[[links]]
name = "Hugo forum post"
url = "https://discourse.gohugo.io/t/hugo-modules-for-dummies/20758"
[[links]]
name = "Scripter intro page"
url = "https://scripter.co/hugo-modules-getting-started/"
+++


Using Hugo modules you can import anything to your site from any public git repo on the internet.

You need to make your site a module and then set the remote repo and the path in your site where you want the content to be mounted.

The remote site will need to be set to public to gain access to it.


## 1. You need Go

You need a recent[^1] version of Go installed on your machine to use Hugo modules. To check take a look at Hugo's environment variables in your terminal:

```bash
hugo env
```

This gives something like this. Line 4 is what we we're interested in.

```bash
hugo v0.120.4-f11bca5fec2ebb3a02727fb2a5cfb08da96fd9df+extended windows/amd64 BuildDate=2023-11-08T11:18:07Z VendorInfo=gohugoio
GOOS="windows"
GOARCH="amd64"
GOVERSION="go1.21.4"
github.com/sass/libsass="3.6.5"
github.com/webmproject/libwebp="v1.2.4"
github.com/sass/dart-sass/protocol="2.3.0"
github.com/sass/dart-sass/compiler="1.69.5"
github.com/sass/dart-sass/implementation="1.69.5"
```

## 2. Your site needs to be a module

In order to use modules your site needs to be a module too.

To do this you need the url of your remote repo[^2] then use it in the command:

```bash
hugo mod init github.com/USER/PROJECT
```

This will create a two line, `go.mod` file in your project containing the remote link of the project and the Go version number. It's easily edited.


## 3. Importing some content

Next, to import some content from somewhere, you need to add a `module` section to the Hugo config file, `hugo.toml`. This section is a list of imports:

```toml
[module]
    [[module.imports]]
    path = "github.com/USER/REPO-NAME " # path to the remote repo to be imported w/out the https://
```

Next set up a `mounts` section in your config file:

```toml
[[module.imports]]
path = "github.com/USER/REPO-NAME"

  [[module.imports.mounts]]
  source = "icons"
  target = "assets/icons"
```

The source part is the part of the remote repo you wish to *mount*. And the `target` is where you wish to mount it in your project.


## Hugo's help file

The `hugo mod` command can do a variety of tasks. See below for info from Hugo's help files. (These can be access from the terminal using `hugo mod subcommand --help`.)

{{< details "hugo mod" >}}

Note that Hugo will always start out by resolving the components defined in the site
configuration, provided by a _vendor directory (if no --ignoreVendorPaths flag provided),
Go Modules, or a folder inside the themes directory, in that order.

See https://gohugo.io/hugo-modules/ for more information.

Usage:
  hugo mod [command]

### Available Commands:

| command  | description                                                |
| :------- | :--------------------------------------------------------- |
| `clean ` | Delete the Hugo Module cache for the current project.      |
| `get   ` | Resolves dependencies in your current Hugo Project.        |
| `graph ` | Print a module dependency graph.                           |
| `init  ` | Initialize this project as a Hugo Module.                  |
| `npm   ` | Various npm helpers.                                       |
| `tidy  ` | Remove unused entries in go.mod and go.sum.                |
| `vendor` | Vendor all module dependencies into the _vendor directory. |
| `verify` | Verify dependencies.                                       |

### Flags:

  -h, --help   help for mod

### Global Flags:

| flag                         | description                                                           |
| :--------------------------- | :-------------------------------------------------------------------- |
| `--clock string            ` | set the clock used by Hugo, e.g. --clock 2021-11-06T22:30:00.00+09:00 |
| `--config string           ` | config file (default is hugo.yaml                                     | json | toml) |
| `--configDir string        ` | config dir (default "config")                                         |
| `--debug                   ` | debug output                                                          |
| `-d, --destination string  ` | filesystem path to write files to                                     |
| `-e, --environment string  ` | build environment                                                     |
| `--ignoreVendorPaths string` | ignores any _vendor for module paths matching the given Glob pattern  |
| `--logLevel string         ` | log level (debug                                                      | info | warn  | error) |
| `--quiet                   ` | build in quiet mode                                                   |
| `-s, --source string       ` | filesystem path to read files relative from                           |
| `--themesDir string        ` | filesystem path to themes directory                                   |
| `-v, --verbose             ` | verbose output                                                        |

Use "hugo mod [command] --help" for more information about a command.

{{</details >}}


{{< details "hugo mod clean" >}}

Delete the Hugo Module cache for the current project.

### Usage:
  hugo mod clean [flags] [args]

### Flags:

| Flag                    | Description                                                             |
| :---------------------- | :---------------------------------------------------------------------- |
| --all                   | clean entire module cache                                               |
| -b, --baseURL string    | hostname (and path) to the root, e.g. https://spf13.com/                |
| --cacheDir string       | filesystem path to cache directory                                      |
| -c, --contentDir string | filesystem path to content directory                                    |
| -h, --help              | help for clean                                                          |
| --pattern string        | pattern matching module paths to clean (all if not set), e.g. "**hugo*" |
| -t, --theme strings     | themes to use (located in /themes/THEMENAME/)                           |

### Global Flags:

| Flag                       | Description                                                           |
| :------------------------- | :-------------------------------------------------------------------- |
| --clock string             | set the clock used by Hugo, e.g. --clock 2021-11-06T22:30:00.00+09:00 |
| --config string            | config file (default is hugo.yaml                                     | json | toml) |
| --configDir string         | config dir (default "config")                                         |
| --debug                    | debug output                                                          |
| -d, --destination string   | filesystem path to write files to                                     |
| -e, --environment string   | build environment                                                     |
| --ignoreVendorPaths string | ignores any _vendor for module paths matching the given Glob pattern  |
| --logLevel string          | log level (debug                                                      | info | warn  | error) |
| --quiet                    | build in quiet mode                                                   |
| -s, --source string        | filesystem path to read files relative from                           |
| --themesDir string         | filesystem path to themes directory                                   |
| -v, --verbose              | verbose output                                                        |


{{</ details >}}



{{< details "hugo mod get" >}}

Resolves dependencies in your current Hugo Project.

Some examples:

Install the latest version possible for a given module:

    `hugo mod get github.com/gohugoio/testshortcodes`
    
Install a specific version:

    `hugo mod get github.com/gohugoio/testshortcodes@v0.3.0`

Install the latest versions of all direct module dependencies:

    `hugo mod get`
    `hugo mod get ./... (recursive)`

Install the latest versions of all module dependencies (direct and indirect):

    `hugo mod get -u`
    `hugo mod get -u ./... (recursive)`

Run `go help get` for more information. All flags available for "go get" is also relevant here.

Note that Hugo will always start out by resolving the components defined in the site
configuration, provided by a _vendor directory (if no --ignoreVendorPaths flag provided),
Go Modules, or a folder inside the themes directory, in that order.

See https://gohugo.io/hugo-modules/ for more information.

### Usage:
  `hugo mod get [flags] [args]`

### Flags:
  `-h, --help   help for get`

### Global Flags:

| Command                    | Decription                                                            |
| :------------------------- | :-------------------------------------------------------------------- |
| --clock string             | set the clock used by Hugo, e.g. --clock 2021-11-06T22:30:00.00+09:00 |
| --config string            | config file (default is hugo.yaml / json / toml)                      |
| --configDir string         | config dir (default "config")                                         |
| --debug                    | debug output                                                          |
| -d, --destination string   | filesystem path to write files to                                     |
| -e, --environment string   | build environment                                                     |
| --ignoreVendorPaths string | ignores any _vendor for module paths matching the given Glob pattern  |
| --logLevel string          | log level (debug error)                                               |
| --quiet                    | build in quiet mode                                                   |
| -s, --source string        | filesystem path to read files relative from                           |
| --themesDir string         | filesystem path to themes directory                                   |
| -v, --verbose              | verbose output                                                        |

{{</details >}}


{{<details "hugo mod init" >}}

Delete the Hugo Module cache for the current project.

Usage:
  hugo mod clean [flags] [args]

### Flags:

| Command                 | Decription                                                              |
| :---------------------- | :---------------------------------------------------------------------- |
| --all                   | clean entire module cache                                               |
| -b, --baseURL string    | hostname (and path) to the root, e.g. https://spf13.com/                |
| --cacheDir string       | filesystem path to cache directory                                      |
| -c, --contentDir string | filesystem path to content directory                                    |
| -h, --help              | help for clean                                                          |
| --pattern string        | pattern matching module paths to clean (all if not set), e.g. "**hugo*" |
| -t, --theme strings     | themes to use (located in /themes/THEMENAME/)                           |

### Global Flags:

| Command                    | Decription                                                            |
| :------------------------- | :-------------------------------------------------------------------- |
| --clock string             | set the clock used by Hugo, e.g. --clock 2021-11-06T22:30:00.00+09:00 |
| --config string            | config file (default is hugo.yaml /json / toml)                       |
| --configDir string         | config dir (default "config")                                         |
| --debug                    | debug output                                                          |
| -d, --destination string   | filesystem path to write files to                                     |
| -e, --environment string   | build environment                                                     |
| --ignoreVendorPaths string | ignores any _vendor for module paths matching the given Glob pattern  |
| --logLevel string          | log level (debug  info / warn  / error)                               |
| --quiet                    | build in quiet mode                                                   |
| -s, --source string        | filesystem path to read files relative from                           |
| --themesDir string         | filesystem path to themes directory                                   |
| -v, --verbose              | verbose output                                                        |


{{</details >}}

{{<details "hugo mod npm" >}}
Various npm (Node package manager) helpers.

### Usage:

  `hugo mod npm` [command] [flags]
  `hugo mod npm` [command]

### Available Commands:

  `pack`        Experimental: Prepares and writes a composite package.json file for your project.

### Flags:

  `-h`, `--help`   help for npm

### Global Flags:

| Command                    | Decription                                                            |
| :------------------------- | :-------------------------------------------------------------------- |
| --clock string             | set the clock used by Hugo, e.g. --clock 2021-11-06T22:30:00.00+09:00 |
| --config string            | config file (default is hugo.yaml                                     | json | toml) |
| --configDir string         | config dir (default "config")                                         |
| --debug                    | debug output                                                          |
| -d, --destination string   | filesystem path to write files to                                     |
| -e, --environment string   | build environment                                                     |
| --ignoreVendorPaths string | ignores any _vendor for module paths matching the given Glob pattern  |
| --logLevel string          | log level (debug                                                      | info | warn  | error) |
| --quiet                    | build in quiet mode                                                   |
| -s, --source string        | filesystem path to read files relative from                           |
| --themesDir string         | filesystem path to themes directory                                   |
| -v, --verbose              | verbose output                                                        |

Use "hugo mod npm [command] --help" for more information about a command.

{{</details >}}


{{<details "hugo mod tidy" >}}

Remove unused entries in go.mod and go.sum.

### Usage:
  hugo mod tidy [flags] [args]

### Flags:

| Command                   | Decription                                               |
| :------------------------ | :------------------------------------------------------- |
| `-b, --baseURL string   ` | hostname (and path) to the root, e.g. https://spf13.com/ |
| `--cacheDir string      ` | filesystem path to cache directory                       |
| `-c, --contentDir string` | filesystem path to content directory                     |
| `-h, --help             ` | help for tidy                                            |
| `-t, --theme strings    ` | themes to use (located in /themes/THEMENAME/)            |

### Global Flags:

| Command                      | Decription                                                            |
| :--------------------------- | :-------------------------------------------------------------------- |
| `--clock string            ` | set the clock used by Hugo, e.g. --clock 2021-11-06T22:30:00.00+09:00 |
| `--config string           ` | config file (default is hugo.yaml / json / toml)                      |
| `--configDir string        ` | config dir (default "config")                                         |
| `--debug                   ` | debug output                                                          |
| `-d, --destination string  ` | filesystem path to write files to                                     |
| `-e, --environment string  ` | build environment                                                     |
| `--ignoreVendorPaths string` | ignores any _vendor for module paths matching the given Glob pattern  |
| `--logLevel string         ` | log level (debug info / warn / error)                                 |
| `--quiet                   ` | build in quiet mode                                                   |
| `-s, --source string       ` | filesystem path to read files relative from                           |
| `--themesDir string        ` | filesystem path to themes directory                                   |
| `-v, --verbose             ` | verbose output                                                        |


{{</details >}}

[^1]: Go version 1.12 or later.

[^2]: Use `git remote -v` to find the 3 parts you need: the TLD, the username, the project nameVarious helpers to help manage the modules in your project's dependency graph.
Most operations here requires a Go version installed on your system (>= Go 1.12) and the relevant VCS client (typically Git).
This is not needed if you only operate on modules inside /themes or if you have vendored them via "hugo mod vendor".




