+++
title = 'Hugo Version'
date = 2023-11-10T10:31:41Z
draft = false
description = "Description for the meta tag"
summary = "How and why my first build on Netlify failed."
page_img = ""
+++

When I first tried my site on Netlify the build failed. The first problem was it couldn't find a config file. I hadn't set a hugo version on Netlify or created a `netlify.toml` file with build instructins. I wanted to see if Netlify would figure it out from the `hugo.toml` file which has the following hugo module:

```toml
[module]
  [module.hugoVersion]
    extended = true
    min = "0.120.4"
```

But the config file in Hugo has changed its name in recent versions from `config.toml` to `hugo.toml` so I'm assuming that's why it couldn't find the config file.

I next added the Hugo version in the Environment Variables settings on Netlify. But this resulted in another fail, this time it was unable to find the Hugo version to build the site. I tried the link to the Hugo GitHub repo and sure enough it didn't work. What to do?

Well finally I created a new `netlify.toml` file with some settings pasted from [the official Hugo docs](https://gohugo.io/hosting-and-deployment/hosting-on-netlify/#configure-hugo-version-in-netlify):

```toml
[context.production.environment]
  HUGO_VERSION = "0.120.4"
  HUGO_ENV           = "production"
  HUGO_ENABLEGITINFO = "true"

[build]
  publish = "public"
  command = "hugo --gc"

  [build.environment]
    HUGO_VERSION = "0.120.4"
```

I don't understand exactly why the second build failed. The url was wrong. Somehow adding the `netlify.toml` file fixed the problem though.