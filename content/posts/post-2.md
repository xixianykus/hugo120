+++
title = 'Izmir Animation Library'
linktitle = 'Izmir Library'
date = 2023-11-13T22:42:44+01:00
draft = false
tags = [ 'CSS', 'animation']
summary = "Some lorem ipsum filler text"
animation = "izmir"
+++

This auto generated post by Hugo has been repurposed to show the [Izmir animation library].



{{< izmir-test >}}

The code required for this, assuming the library is linked to the document, is:

```html
<figure class="c4-izmir">
  <img src="https://source.unsplash.com/FaPxZ88yZrw/600x400" alt="Sample Image">
  <figcaption>
    <h3>This uses the Izmir animation library. This text in the h3</h3>
    <p>And more in a paragraph.</p>
  </figcaption>
</figure>
```




[Izmir animation library]: https://ciar4n.com/izmir/