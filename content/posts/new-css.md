+++
title = 'New CSS in 2023'
date = 2023-11-24T15:09:32Z
draft = false
description = "Some of the new features in CSS in 2023"
summary = "Some of the new features in CSS in 2023"
tags = ["CSS"]
page_img = ""
+++

Here's a quick list of new features in CSS that came in this year (2023).


## CSS nesting

All browsers now support CSS nesting and Firefox even supports a more *relaxed* syntax for nesting.

Here's an example from this site's CSS:

```css
main {
	background-color: var(--white);
	padding-block: 4em;
	display: grid;
	grid-column: 2 / -2;
	grid-row: span 99;
	grid-template-columns: subgrid;
	border-block: solid #891010 2em;

  & p, ul, ol {
    grid-column: 1 / -1;
  }
  & .content a {
    text-decoration: underline;
    text-decoration-color: var(--red);
    text-decoration-thickness: 2px;
  }
}
```


## The parent selector

The seletor `:has` is just about to come out in Firefox, the last browser to support it.

This has many useful applications.

```css
p:has(img) {
  grid-column: 1 / -1;
}
```

## Text Wrap

The new `text-wrap` proerty takes several values, most useful being `balance` for headings and `pretty` for longer blocks of text to avoid *orphans*. It takes several other values but these may not be working at the moment.

## Colours

Several enhancement to colours have come along this year:

- `color-mix()`
- The LCH and LAB colour modes.
- relative colours

The first of these allows you to mix colours together.

```css
root: {
    --accent: #778ffe;
    --accent-2: #892ec1;

    --accent-3: color-mix(var(--accent) var(--accent-2));
}
```

This is supported by the main 3 browsers.

## Container Queries



## Cascade layers

## Trigonometry functions

## Subgrid

The `subgrid` value for `grid-template-columns` and/or `grid-template-rows` allows children of a grid layout to use the same grid. This is a huge improvement and although Firefox came out with 4 years ago Chrome and Safari only caught up this year. See the example [above](#css-nesting) to see this in action.

## view transitions

Only partial support in Chrome so far. This will allow multi page sites appear like single page apps.

## animation-timeline: scroll()

Will allow animations to be linked to scrolling negating the need for JS libraries to do the same thing. Currently only in Chromium browers, though coming to Firefox soon.

```css

.element {
    animation: gipbop 0.5s;
    animation-timeline: scroll();
} 
```

## Scoped CSS

This is only available in Chromium browsers.