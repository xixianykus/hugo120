---
title: Gitlab with https
date: 2023-11-10T07:55:11.000Z
draft: false
summary: How I failed at https connection to push to GitLab
description: My attempt at using GitLab with https instead of ssh
page_img: /img/golf-moodboard-3.png
tags: [Gitlab]
---
So using GitHub I've often used an https connection to push/pull to the remote repo without issue. I find this simpler and more convenient and for general projects they don't require the additional security of an ssh connection. So I tried the same with GitLab.

When setting up a repo GitLab never suggests an option to use https however I found a [video](https://www.youtube.com/watch?v=qN6EeASfVT0) here and followed the instructions.

Grab the https url from *Clone* button dropdown and add it as a remote:

```bash
git remote add gitlab https://gitlab.com/<repo-name>/<project-name>.git

git push -f -u gitlab master
```

Unfortunately I got the response:

> `The project you were looking for could not be found or you don't have permission to view it.`
>
> `fatal: repository 'https:.... ' not found`

When I connected using ssh I had no problem. 

So the question remains: is there a way to set up a connection to GitLab using only https, as in the [video](https://www.youtube.com/watch?v=qN6EeASfVT0)?