+++
title = 'CSS Gradient Reference'
date = 2023-11-27T08:17:30Z
draft = false
description = "An overview of the syntax for CSS gradients"
summary = "An overview of the syntax for CSS gradients"
tags = ["CSS"]
page_img = ""
style = 'h1 {color: transparent; background-image: linear-gradient(black 30%, red 90%); background-clip: text;}'
+++


CSS gradients are surprisingly sophisticated and can be used to create all kinds of patterns. This means the syntax is inevitably pretty complicated too. This article just covers basic, single gradients. Though to increase the complexity even more it's possible to use multiple gradients stacked on top of one another using some transparency or blend modes.

## Basic types

There are 3 main types of gradient all of which are defined with the `background-image` property:

1. linear  `linear-gradient(red, blue)`
2. radial  `radial-gradient(red, blue)`
3. conic  `conic-gradient(red, blue)`

A basic linear gradient runs from top to bottom or to put it another way at an angle of 180 degrees.

{{< gradient "linear-gradient(red, blue)" >}}


A basic radial gradient moulds to fit the available space. 

{{< gradient "radial-gradient(red, blue)" >}}


A basic conic gradient which doesn't look that useful in it's basic form.

{{< gradient "conic-gradient(red, blue)" >}}


Additionally the type of gradient can be prefaced with the word `repeating` but these are a bit more complex. See [repeating gradients](#repeating-gradients) below.



## Linear gradients

The first thing you can do is add an angle. As the default, top to bottom, is considered 180 degrees to shift the angle further right by 20 degrees we add that to the 180 degree default to get 200 degrees.

{{< gradient "linear-gradient(200deg, red, blue)" >}}

As this is a little clumsy you can also use the keyword `to` like so:

{{< gradient "linear-gradient(to top right, red, orange)" >}}

Note this is NOT the same as `45deg` unless the element is square. The angle runs from one corner the to the other so the angle will vary depending on it's shape.

### Colour stops

The next thing you can do is add colour stops;

{{< gradient "linear-gradient(red 30%, tan 50% )" >}}

The red stop starts at 50% but since it is the first colour stop everything before it is also red. The tan stop begins at 50% so everything after it is tan.

The colour stop percentage values say how far along on the line of the gradient a colour is. This is easy to see on a dynamic version like [cssgradient.io](https://cssgradient.io/).

If both values are the same you get a hard edge.

{{< gradient "linear-gradient(to right, peru 50%, navy 50%)" >}}


So making a basic flag is easy.

{{< gradient "linear-gradient(black 33%, red 33% 66%, gold 66%)" >}}

Note the use of a starting and finishing stop for the red colour. This is to create a hard edge where it meets the gold colour. For the first and last colours you don't need 0% and 100% stops as these are assumed.

{{< gradient "linear-gradient(to top right, navy 12.5%, plum 12.5% 25%, peru 25% 37.5%, teal 37.5% 50%, pink 50% 62.5%, grey 62.5% 75%, gold 75% 87.5%, blue 87.5% )" >}}


You can also set gradient stops in other CSS units like pixels.

{{< gradient "linear-gradient(red 80px, blue 80px 150px, red 150px)" >}}

## Radial gradients

To create a circular gradient you can preface with the key word `circle`:

```css
background-image: radial-gradient(circle, red, blue)
```
{{< gradient "radial-gradient(circle, red, blue)" >}}


Using the `at` keyword you can position the gradient.

{{< gradient "radial-gradient(circle at top center, red, blue)" >}}

You can use percentage values too. So instead of `top center` you would say `50% 0%` using the *x* value first then the *y* one.

{{< gradient "radial-gradient(circle at 50% 0%, red, blue)" >}}


Just like linear gradients you can apply colour stops:

{{< gradient "radial-gradient(circle at top center, red 20%, blue 30%)" >}}

### Sizing radial gradients

Other keywords can be used to set the size:

- closest-side
- farthest-side
- closest-corner
- farthest-corner

{{< gradient "radial-gradient(circle closest-side, red, blue)" >}}

You can also specify perctage values.

{{< gradient "radial-gradient(circle closest-side at 90% 30%, red, blue)" >}}









## Repeating gradients

You can put a `repeating-` in front of any the main gradients to get a repeating pattern.

{{< gradient "repeating-linear-gradient(red, blue)" >}}

However you need to position the colour stops to get an effect.

{{< gradient "repeating-linear-gradient(to right, red 0% 10%, blue 10% 20%)" >}}

NB. This requires a start and end stop for each colour.

{{< gradient "repeating-radial-gradient(circle, teal 0% 10%, plum 10% 20%)" >}}


For conic gradients you generally need low percentage values.

{{< gradient "repeating-conic-gradient(at 50% 90%, teal 0% 2.5%, tan 2.5% 7.5%)" >}}

The `at` keyword is used on it's own.

## Gradients on text

To add any gradient to a piece of text first add it as a background:

```css
h1 {
    background-image: linear-gradient(red, blue);
}
```

Next make the text transparent so you can see the gradient.

```css
h1 {
    background-image: linear-gradient(red, blue);
    color: #0000;
}
```

Finally clip the background to the text using `background-clip`.

```css
h1 {
    background-image: linear-gradient(red, blue);
    color: #0000;
    background-clip: text;
}
```