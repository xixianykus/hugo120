---
date: "2023-11-13T10:42:05Z"
description: How to get animations on a website.
draft: false
page_img: ""
summary: There are a number of ways to create animations for websites. This article
  will explore the posibilities.
tags:
- CSS
- Javascript
title: Animations
---

## 1. Write your own animation code

## 2. Use an animation library

### animate.css
There are several of these around. The most famous and probably most used is animate.css found at [animate.style]. This is a whole CSS file of all the animations. So with this you either download the `animate.css` file or add link to the CDN version. Animations can then be browsed on the web site and you simply add 2 class names to the element or elements you wish the animimation to apply to.

```html
<h1 class="animate__animated animate__bounce">An animated element</h1>
```

Alternatively you can add the animation to your element directly in your CSS.

To customize the animations you can either use the quicker though more limited option of using the in built utility classes or you can change the timing in your own CSS file by using the CSS custom properties that are set.


### animista

Another good one is [animista.net] by Ana Travas. This groups animations: Basic, Entrances, Exits, Text, Attention and Background. You can tweak each animation in the settings and when done you click the generate code button for the single animation.

This provides a nice lightweight solution: you're downloading tons of code you're not going to use.

### animatiss

[Animatiss] provedes another quick and easy way to grab CSS animations. Click one button to play and another to grab the code.

### Izmir

The [Izmir animation library] is specifically for hover effects over images. The docs cover everything but note there is [a CDN link] available too.

### Anim XYZ

[Anim XYZ] claims to be:

> The first composable CSS animation toolkit.

It's built using CSS Custom properties. And animations are added using class names and the `xyz` attribute.

### wow.js

Wow.js is for activating animate.css animations on scroll. Quick and easy to use. Just link to the libraries, either via CDN or downloading then add class names and Wow tags to the HTML elements you wish to animate. See a [demo](https://codepen.io/Synphod/pen/ExpgXBg).

### Free Front End

[Free Front End] has demos and links to 90 different animations, most of which are on Codepen.

There are of course new animations being added to Codepen every day but finding them can be tricky so this is a limited though useful resource.


[animate.style]: https://animate.style/
[animista.net]: https://animista.net/
[Animatiss]: 
[Anim XYZ]: https://animxyz.com/
[Free Front End]: https://freefrontend.com/css-animation-examples/
[Izmir animation library]: https://ciar4n.com/izmir/
[a CDN link]: https://www.jsdelivr.com/package/npm/@ciar4n/izmir