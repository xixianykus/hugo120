+++
title = 'Hugo Config File'
date = 2023-11-04T20:28:04Z
draft = false
tags = ["config file", "Hugo"]
summary = "The default theme's config file"
+++

THe default theme contains a config file. This won't work while it's sitting in the `/themes` folder but you can either move it or  cut'n'paste the contents into the hugo.toml file at the root of your project.


```toml
baseURL = 'https://example.org/'
languageCode = 'en-us'
title = 'My New Hugo Site'

[[menus.main]]
name = 'Home'
pageRef = '/'
weight = 10

[[menus.main]]
name = 'Posts'
pageRef = '/posts'
weight = 20

[[menus.main]]
name = 'Tags'
pageRef = '/tags'
weight = 30

[module]
  [module.hugoVersion]
    extended = false
    min = "0.116.0"
```