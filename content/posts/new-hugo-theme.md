+++
title = 'New Hugo Theme'
date = 2023-11-04T07:35:17Z
summary = "A look at the code in the new default Hugo skeleton theme."
+++


Using the `hugo new theme` command now provides a much more developed Hugo skeleton theme than previously making it very easy to set up a new Hugo site:


```bash
hugo new site Sitename
cd Sitename
hugo new theme MyTheme
echo 'theme = "MyTheme"' >> hugo.toml
cp -r themes/MyTheme/content/* content
```

Then just run hugo server and go to `localhost:1313` in your browser to see the new site.

## The new skeleton theme

Hugo's new default skeleton theme is simple yet has some carefully written and rather advanced code. It's worth taking a look at because it gives a clue into some good practices when using Hugo.

## Using new theme to create a site

A very quick way to create a new site is with the `hugo new theme command`. Since a theme on it's own is typically a fully working site you just use that instead of `hugo new site`. When creating a new theme by default Hugo will create it in the themes directory but you don't want this as there is no themes directory. Instead you set the the themes directory to the current directory using the dot: 

```bash
hugo new theme <sitename> --themesDir .
```

This will create a new folder called whatever you used for `<sitename>` and place the theme (a working site) there.

All that's left is then to `cd` into the folder and run `hugo serve`


## Layouts

The `/layouts` folder contains Hugo's HTML templates to build the site. The first difference I noticed was that this contained no `index.html` file for the home page. Instead there is a file `_default/home.html`. In the [template lookup order](https://gohugo.io/templates/lookup-order/#home-page) this ranks much lower than index.html:

1. layouts/index.html.html
1. layouts/home.html.html
1. layouts/list.html.html
1. layouts/index.html
1. layouts/home.html
1. layouts/list.html
1. layouts/_default/index.html.html
1. layouts/_default/home.html.html
1. layouts/_default/list.html.html
1. layouts/_default/index.html
1. **layouts/_default/home.html**
1. layouts/_default/list.html

Keeping the home page template in the `layouts/_default` directory is probably more intuitive, at least to new users. This folder also contains the list page template `list.html`, content page template `single.html` and a `baseof.html` template for the basic HTML page structure of all pages.

Also in `themes/themename/layouts` is a folder of partials. This includes partials for:

- the footer `footer.html`
- the head section `head.html`
- the header `header.html`
- the main navigation menu `menu.html`
- a section for taxonomy terms like tags or categories `terms.html`

The head section uses 2 additional partials stored in a `head` subfolder for CSS and Javascript, `css.html` and `js.html`.

## TOML frontmatter

Another change is from the default language for frontmatter from YAML to TOML. 

