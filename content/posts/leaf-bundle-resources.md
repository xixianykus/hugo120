+++
title = 'Leaf Bundle Resources'
date = 2023-11-11T16:32:52Z
draft = false
description = "Description for the meta tag"
summary = "No summary for this page yet"
tags = ["Hugo"]
page_img = ""
+++


In Hugo resources can refer to global resources that are available to the whole site, local resources that are available to just one page and remote resources that exist somewhere else on the internet.

This article is focusing on local resources that exist in a folder with an `index.md` file along with any other files or subfolders to be used by the page. This is known as a *leaf bundle*.

Whereas global resources are accessed with the `resourses` function these local resources are accessed with `.Resources`, that is capital `R` after a dot.


###  1. `.Resources.ByType`  (collection)

This grabs a collection of media defined by a type.
A *type* needs to be suggested. It could be `image`


### `.Resources.Get`  (single file)

This grabs a single file defined by a path:

```go-html-template
{{ with .Resources.Get "img/photo.jpg" }}
    <img src="{{ .RelPermalink }}" width="{{ .Width }}" height="{{ .Height }}" alt="">
{{ end }}
```

### `.Resources.GetMatch` (single file)

If you don't know the exact path this will grab the first file that matches a given pattern.


### `.Resources.Match` (collection)

Like above but will get all files matching the given pattern.