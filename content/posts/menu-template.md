+++
title = 'Menu Template'
date = 2023-11-04T20:36:05Z
draft = false
summary = "A look at the rather sophisticated menu template"
tags = ["hugo"]
+++


So I first tried to add a new section by adding it to the menu section in `hugo.toml`. It appeared on the menu but didn't work. The problem? There was no `_index.md` page for it. Didn't Hugo generate a list page if one wasn't referenced as a content file? Hmmm.

The partial `menu.html` is pulled into the `header.html` partial below the title of the site. Before looking at the menu it's worth taking a looker at the header partial. 

The link to the menu partial looks like this:

```go-html-template
{{ partial "menu.html" (dict "menuID" "main" "page" .) }}
```

So rather than just passing in the usual context, the dot, this passes in a dictionary with two entries:

```yaml
menuID: "main"
page: "."
```

These are used at the very start of the `menu.html` partial:


```go-html-template
{{- /*
Renders a menu for the given menu ID.

@context {page} page The current page.
@context {string} menuID The menu ID.

@example: {{ partial "menu.html" (dict "menuID" "main" "page" .) }}
*/}}

{{- $page := .page }}
{{- $menuID := .menuID }}

{{- with index site.Menus $menuID }}
  <nav>
    <ul>
      {{- partial "inline/menu/walk.html" (dict "page" $page "menuEntries" .) }}
    </ul>
  </nav>
{{- end }}

{{- define "partials/inline/menu/walk.html" }}
  {{- $page := .page }}
  {{- range .menuEntries }}
    {{- $attrs := dict "href" .URL }}
    {{- if $page.IsMenuCurrent .Menu . }}
      {{- $attrs = merge $attrs (dict "class" "active" "aria-current" "page") }}
    {{- else if $page.HasMenuCurrent .Menu .}}
      {{- $attrs = merge $attrs (dict "class" "ancestor" "aria-current" "true") }}
    {{- end }}
    {{- $name := .Name }}
    {{- with .Identifier }}
      {{- with T . }}
        {{- $name = . }}
      {{- end }}
    {{- end }}
    <li>
      <a
        {{- range $k, $v := $attrs }}
          {{- with $v }}
            {{- printf " %s=%q" $k $v | safeHTMLAttr }}
          {{- end }}
        {{- end -}}
      >{{ $name }}</a>
      {{- with .Children }}
        <ul>
          {{- partial "inline/menu/walk.html" (dict "page" $page "menuEntries" .) }}
        </ul>
      {{- end }}
    </li>
  {{- end }}
{{- end }}
```