+++
title = 'Decap Setup of git-gateway on Gitlab'
linktitle = 'Decap Setup'
date = 2023-11-10T05:24:19Z
draft = false
description = "How to set up Decap CMS on a Hugo site hosted on Netlify."
summary = "How to set up Decap CMS on a Hugo site hosted on Netlify."
tags = ["Decap"]

[[links]] 
name = "Git-gateway on Decap docs"
url= "https://decapcms.org/docs/git-gateway-backend/"
[[links]]
name = "Git Gateway on Netlify"
url = "https://docs.netlify.com/security/secure-access-to-sites/git-gateway/"
[[links]]
name = "Add Identity users"
url = "https://docs.netlify.com/security/secure-access-to-sites/identity/registration-login/#add-identity-users"
[[links]]
name = "Gitlab Access Tokens page"
url = "https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html"
+++


Decap CMS, formally Netlify CMS, is a great option to use on Netlify because Netlify has a couple of things that make setting up Decap a breeze. First they have Oauth which can be used to authorize user access via GitLab or GitHub. Secondly they make setting up `git-gateway` easy, using Netlify's *Identity*. This allows you to set up CMS access for users without needing a GitHub or GitLab account. Simply sign up with email and log in with a secure passphrase.


## The 3 Areas of Setup

There are 3 places to go to setup Decap CMS with Gitlab: in your projects files, on Gitlab and on Netlify.

### 1. On your Hugo site

1. Create a folder (`/static/admin/`) with 2 files (`index.html` and `config.yml`). Add some code to these.
2. Add 4 script tags to your site. Two of these go in the newly created `static/admin/index.html`. One in the `<head>` section is the Netlify identity widget. The other is a CDN link for the React app that is the CMS. Then you need 2 more on your sites home page both for Netlify identity. Again one is in the head section and the other is in the body.

### 2. On Gitlab

Not as complicated as it looks:

1.  Sign in to Gitlab and open your project.
2.  In the left hand menu, near the bottom, click on the settings dropdown then select *access tokens*.
3.  Click the `Add new token` button on the right.
4.  Give it a name. Anything will do.
5.  If you want less than a year put in an expiry date. If not skip the `Expiration date` box.
6.  Click on the select a role and choose `Owner`.
7.  Tick the following scopes' boxes: `api`, `read_api`, `read_repository` and `write repository`.
8.  Click `Create project access token` and copy the token to the clipboard.
9.  Go to netlify.com and sign into your account and go to your project.

### 3. On Netlify

Sign into your account and go to your project's page.

1. Find your project and click on `site configuration` in the left hand side bar.
2. In the vertical menu that appears click on `Identity`, near the bottom.
3. Scroll down or click on `Services`, then `Git-gateway`.
4. Either `Enable Git Gateway` or `Edit settings` if it's already enabled.
5. Paste in the new access token from Gitlab.

You now have to invite users to join. You this by going to the `Integrations` section on Netlify. The link to it is in the lefthand sidebar again, about halfway down. In the submenu that appears click on `Identity` and then on `View` in the Netlify Identity box. From click on `Invite users` and add some emails of those people you wish to invite. The maximum number is 5 on Netlify's free tier.

## Downside of Gitlab

One problem encountered with Gitlab was the need for an access token. In itself that's not a big deal. However Gitlab's access token's have to have an expiry date and the maximum length is only one year. Definitely too short for something like CMS access.

To create or renew your access is not too hard. 



**In netlify**




## 1. On your local machine

1. Create a directory `static/admin` in your Hugo site.
2. Add two files to it: index.html and config.yml
3. In index.html create a simple html page and add one of these script tags to the body section:
   ```html
   <script src="https://unpkg.com/decap-cms@^3.0.0/dist/decap-cms.js"></script>
     or
   <script src="https://cdn.jsdelivr.net/npm/decap-cms@^3.0.0/dist/decap-cms.js"></script>
   ```

> GitLab access token: grants permission to the linked repository. You must create a personal access token in GitLab with the necessary scopes for Git Gateway to act on users’ behalf. For example, if you set up Git Gateway to allow users to edit site content without write access to the GitLab repository or even a GitLab account, then your access token needs these scopes: api, read_api, read_repository, and write_repository.
> &mdash; [Netlify docs](https://docs.netlify.com/security/secure-access-to-sites/git-gateway/#app)


## Next prob

Messge from the CMS..

> 403 Forbidden: You are not allowed to push into this branch.

So the solution was to create a new access token in Gitlab with the level of owner rather than *guest*.


[^1]: The first time I tried I left the default which was guest and I couldn't post.