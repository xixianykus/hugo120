---
title: Post 1
date: 2023-01-15T16:00:00.000Z
draft: false
summary: This post uses a featured image from the assets directory!
tags:
  - red
page_img: /img/beach-kettlebells-1-1.png
---

To get images working properly I probably need to set up [markdown render hooks].

![Swan](img/swan.png "Where is it then?")

Occaecat nulla excepteur dolore excepteur duis eiusmod ullamco officia anim in voluptate ea occaecat officia. Cillum sint esse velit ea officia minim fugiat. Elit ea esse id aliquip pariatur cupidatat id duis minim incididunt ea ea. Anim ut duis sunt nisi. Culpa cillum sit voluptate voluptate eiusmod dolor. Enim nisi Lorem ipsum irure est excepteur voluptate eu in enim nisi. Nostrud ipsum Lorem anim sint labore consequat do.



[markdown render hooks]: https://learninghugo.netlify.app/posts/markdown/