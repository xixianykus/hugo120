+++
title = 'Lookup Order'
date = 2023-11-11T17:03:45Z
draft = false
description = "Hugo's template lookup order"
summary = "The look up order for single page templates"
tags = ["Hugo"]
page_img = ""
+++


On the offical Hugo page the look up order, even for one template like the single page template here, seems very long.

However by looking at just one part of it we can see how it works as a whole. Here we're looking at single page layouts in the `/_default` directory.

In fact there are only 4 possibilities. However you can expand this indefinitely by naming the template something else and then calling it the frontmatter using the built in `layout` key:

```yaml
layout: big-columns
```

The above will use the `bit-columns.html` template.

## The 4 template files

So for files in the `_default directory`:

Those higher up are selected before those lower down.

1. `layous/_default/demolayout.html.html`
2. `layous/_default/single.html.html`
3. `layous/_default/demolayout.html`
4. `layous/_default/single.html`

For any pages in another directory, say the `layouts/news` the order is the same but because they're a directory specific to a content directory they're always chosen first, before those in the `layous/_default` directory.