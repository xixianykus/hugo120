---
title:  News Layouts
date:  2023-11-11T18:23:27Z
draft:  false
description: "CSS layouts"
summary:  "Getting the layout right is tricky."
page_img:  ""
---

Getting the layouts is tricky. After looking around I considered an old fashioned [column layout] might be best. But there was one thing I had overlooked. You can adjust the span of an element inside a column but the only values are `all` or `none`. There is no spanning just 2 columns, or 3 or 4. 

So that doesn't fit at all with what a genuine newspaper would look like. These tend to vary the column span depending on how the 





[column layout]: https://developer.mozilla.org/en-US/docs/Web/CSS/column-span