---
title:  What will be in the news section?
linktitle: What?
date:  2023-11-11T10:17:51Z
draft:  false
description: "Another post in the Hugo 120 news section"
summary:  "The news section will cover the common themes of Hugo 120 but will be more diverse too."
page_img:  ""
---

The Hugo 120's mission statement, if you will, is clearly laid out in the README file in the project's git repo. But rather than make anyone head over there you can also read the exact same document here on the [About page]





[About page]: /about/