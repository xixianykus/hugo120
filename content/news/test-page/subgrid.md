---
title:  'Subgrid'
date:  2023-11-10T05:19:47Z
draft:  false
description:  "CSS subgrid is finally available in all the major browsers."
summary:  "Something about subgrid being in all the browsers now."
---

It's been a long time coming but from October 2023 CSS subgrid is finally available in all the major 3 browsers: Firefox, Chromium, and Safari.

Firefox had CSS subgrid working way back in 2019 but the other two browsers have taken 4 years to catch up.

It's finally here and now can be used to revolutionize layouts, the same way grid did back 20??.

The main layout of this site uses `subgrid` as does the currently rather messy news section.