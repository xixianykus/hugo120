---
title:  'Intro'
date:  2023-11-11T09:46:48Z
draft:  false
description:  "An intro to the news section"
summary:  "The first bit of text in the new news section"
page_img:  "feature-grenade.jpg"
weight: 10
feature: true
---


The news section was created to try using the CSS `subgrid` layout. This was available in Firefox in 2019 but Chromium browsers dragged their feet for 4 years and subgrid didn't make it into Google Chrome until October 2023. Finally `subgrid` could be used on production sites and by November 2023 around 80% of internet users had browsers that could render subgrid layouts.

Subgrid is an extremely useful addition to the earlier and widely adopted grid layout. However it became immediately apparent that grid was limited because you couldn't place a child's children on the same grid.

Imagine you have a `<header>` section in say a 12 column grid. You decide span that header right across the page taking up 10 of those columns. However as soon as you add anything into your header you have lost access to the original grid. This is where subgrid comes into play. You set your header to `display: grid` and 