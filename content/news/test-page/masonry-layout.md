---
title:  Masonry Layout
date:  2023-11-11T16:22:29Z
draft:  false
description: "Another post in the Hugo 120 news section"
summary:  "Using old fashioned CSS columns might be the best solution to create a varied masonry layout for the news section."
tags: [CSS]
page_img:  ""
---

According the [this article] on CSS Tricks there are many ways to create a masonry layout, depending one which way they flow. However for the use case here using columns might actually be the best way.







[this article]: https://css-tricks.com/piecing-together-approaches-for-a-css-masonry-layout/